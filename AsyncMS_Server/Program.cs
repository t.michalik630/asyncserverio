﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

using MyServer;
// State object for reading client data asynchronously  
public class StateObject
{
    // Size of receive buffer.  
    public const int BufferSize = 1024;

    // Receive buffer.  
    public byte[] buffer = new byte[BufferSize];
    
    // Received data string.
    public StringBuilder sb = new StringBuilder();

    // Client socket.
    public Socket workSocket = null;

    
    public string loggedUser = "";
}

public class AsynchronousSocketListener
{
    // Thread signal.  
    public static ManualResetEvent allDone = new ManualResetEvent(false);

    public AsynchronousSocketListener()
    {
    }

    public static void StartListening()
    {
        // Establish the local endpoint for the socket.  
        // The DNS name of the computer  
        // running the listener is "host.contoso.com".  
        IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
        IPAddress ipAddress = ipHostInfo.AddressList[0];
        IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);

        // Create a TCP/IP socket.  
        Socket listener = new Socket(ipAddress.AddressFamily,
            SocketType.Stream, ProtocolType.Tcp);

        Console.WriteLine("Endpoint.Address : " + localEndPoint.Address);
        Console.WriteLine("Endpoint.AddressFamily : " + localEndPoint.AddressFamily);
        Console.WriteLine("Endpoint.Port : " + localEndPoint.Port);
        Console.WriteLine("Endpoint.ToString() : " + localEndPoint.ToString());

        // Bind the socket to the local endpoint and listen for incoming connections.  
        try
        {
            listener.Bind(localEndPoint);
            listener.Listen(100);

            while (true)
            {
                // Set the event to nonsignaled state.  
                allDone.Reset();

                // Start an asynchronous socket to listen for connections.  
                Console.WriteLine("Waiting for a connection...");
                listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                // Wait until a connection is made before continuing.  
                allDone.WaitOne();
            }

        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        Console.WriteLine("\nPress ENTER to continue...");
        Console.Read();

    }

    public static void AcceptCallback(IAsyncResult ar)
    {
        // Signal the main thread to continue.  
        allDone.Set();

        // Get the socket that handles the client request.  
        Socket listener = (Socket)ar.AsyncState;
        Socket handler = listener.EndAccept(ar);

        // Create the state object.  
        StateObject state = new StateObject();
        state.workSocket = handler;
        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
    }

    public static void ReadCallback(IAsyncResult ar)
    {
        String content = String.Empty;
        String parsedContent = String.Empty;
        String output = String.Empty;

        string[] credentials;
        // Retrieve the state object and the handler socket  
        // from the asynchronous state object.  
        StateObject state = (StateObject)ar.AsyncState;
        Socket handler = state.workSocket;

        // Read data from the client socket.
        int bytesRead = handler.EndReceive(ar);

        
        

        if (bytesRead > 0)
        {
            // There  might be more data, so store the data received so far.  
            state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

            // Check for end-of-file tag. If it is not there, read
            // more data.
            content = state.sb.ToString();
            if(content != "\r\n")
            {
                if (Server.operation == 0)
                {

                    if (content.IndexOf("<EOF>") > -1)
                    {
                        handler.Shutdown(SocketShutdown.Both);
                        handler.Close();
                    }
                    else if (content.IndexOf("1") > -1)
                    {
                        Server.operation = 1;
                        Send(handler, "Insert login and password in pattern -> [login]:[password]\r\n");


                        state.sb.Clear();
                    }
                    else if (content.IndexOf("2") > -1)
                    {
                        Server.operation = 2;
                        Send(handler, "Insert login and password in pattern -> [login]:[password]\r\n");

                        state.sb.Clear();
                    }
                    else if (content.IndexOf("3") > -1)
                    {
                        if (state.loggedUser != "")
                        {
                            Send(handler, state.loggedUser);
                        }
                        else
                        {
                            Send(handler, "Not logged in\r\n");
                        }
                        state.sb.Clear();
                    }
                    else if (content.IndexOf("4") > -1)
                    {
                        
                        if (state.loggedUser != "")
                        {
                            Server.operation = 4;
                            Send(handler, "How old are you?");
                        }
                        else
                        {
                            Send(handler, "Not logged in\r\n");
                        }
                        state.sb.Clear();
                    }
                    else if (content.IndexOf("5") > -1)
                    {
                        if (state.loggedUser != "")
                        {
                            foreach (KeyValuePair<string, bool> user in Server.usersIfOlder)
                            {
                                output = String.Format("Is {0} older than author? - {1}\r\n", user.Key, user.Value);
                                Send(handler, output);
                            }
                            
                        }
                        else
                        {
                            Send(handler, "Not logged in\r\n");
                        }
                        state.sb.Clear();
                    }
                    else
                    {
                        Send(handler, "Press:\r\n1. Login\r\n2. Register\r\n3. Whoami\r\n4. Determine if you're older than author\r\n5. Display other users");
                    }
                }
                else if (Server.operation == 1)
                {
                    parsedContent = content.Replace("\r\n", string.Empty);
                    credentials = parsedContent.Split(":");

                    if (Server.login(credentials[0], credentials[1]))
                    {
                        Console.WriteLine("Successfully logged user {0}", credentials[0]);
                        Send(handler, "Successful login\r\n");
                        state.loggedUser = credentials[0];
                    }
                    else
                    {
                        Console.WriteLine("Failed to log user {0}", credentials[0]);
                        Send(handler, "Failed to login\r\n");
                    }

                    Server.operation = 0;
                    state.sb.Clear();
                }
                else if (Server.operation == 2)
                {

                    parsedContent = content.Replace("\r\n", string.Empty);
                    credentials = parsedContent.Split(":");

                    //Console.WriteLine("login: {0}, password: {1}", credentials[0], credentials[1]);
                    if (Server.addUser(credentials[0], credentials[1]))
                    {
                        Console.WriteLine("Successfully added user {0}", credentials[0]);
                        Send(handler, "Successful registration\r\n");
                    }
                    else
                    {
                        Console.WriteLine("Failed to added user {0}", credentials[0]);
                        Send(handler, "Failed to register\r\n");
                    }

                    Server.operation = 0;
                    state.sb.Clear();
                    Server.displayUsers();
                }
                else if (Server.operation == 4)
                {
                    parsedContent = content.Replace("\r\n", string.Empty);
                    int number;
                    bool success = Int32.TryParse(parsedContent, out number);

                    if (success)
                    {
                        if(Server.olderThanAuthor(state.loggedUser, number))
                        {
                            Send(handler, "You're older\r\n");
                        }
                        else
                        {
                            Send(handler, "You're younger\r\n");
                        }
                    }

                    Server.operation = 0;
                    state.sb.Clear();
                    Server.displayUsers();
                }

            }
            
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
        }
    }

    private static void Send(Socket handler, String data)
    {
        // Convert the string data to byte data using ASCII encoding.  
        byte[] byteData = Encoding.ASCII.GetBytes(data);

        // Begin sending the data to the remote device.  
        handler.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), handler);
    }

    private static void SendCallback(IAsyncResult ar)
    {
        try
        {
            // Retrieve the socket from the state object.  
            Socket handler = (Socket)ar.AsyncState;

            // Complete sending the data to the remote device.  
            int bytesSent = handler.EndSend(ar);
            Console.WriteLine("Sent {0} bytes to client.", bytesSent);

            //handler.Shutdown(SocketShutdown.Both);
            //handler.Close();

        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public static int Main(String[] args)
    {
        StartListening();
        return 0;
    }
}