﻿using System;
using System.Collections.Generic;

namespace MyServer
{
    public static class Server
    {
        public static Dictionary<string, string> users = new Dictionary<string, string>();
        public static Dictionary<string, bool> usersIfOlder = new Dictionary<string, bool>();
        public static int operation = 0; //0-menu, 1-login, 2-register

        public static bool addUser(string user, string password)
        {
            if (users.ContainsKey(user))
            {
                return false;
            }
            else
            {
                users.Add(user, password);
                return true;
            }
        }
        public static bool login(string user, string password)
        {
            if(users.ContainsKey(user))
            {
                if(users[user] == password)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public static void displayUsers()
        {
            foreach (KeyValuePair<string, string> user in users)
            {
                Console.WriteLine("Key = {0}, Value = {1}", user.Key, user.Value);
            }
        }

        public static bool olderThanAuthor(string user, int age)
        {
            if (age > 22)
            {
                usersIfOlder.Add(user, true);
                return true;
            }
            else
            {
                usersIfOlder.Add(user, false);
                return false;
            }
        }
    }
}
